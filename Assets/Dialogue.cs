﻿using System;
using Foggzie.Unity.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public class Sequence
    {
        public string[] Data;
        public int Index;

        public string Current => Data[Index];
        public bool OnLast => Index == Data.Length - 1;

        public Action OnFinish;
    }

    [SerializeField]
    private GameObject navigation;

    [SerializeField]
    private GameObject chefHologram;

    [SerializeField]
    private GameObject dialogueBackground;

    public Text text;
    public GameObject inputBlocker;

    private Sequence _sequence;

    private bool _canRecurse;
    private bool _recursed;

    private bool _lastActiveState;

    public void Display(
        Sequence sequence)
    {
        if (_canRecurse)
        {
            _recursed = true;
        }
        else
        {
            _recursed = false;
        }
        _sequence = sequence;
        text.text = _sequence.Current;
        UpdateActiveStates(true);
    }

    public void StepSequence()
    {
        UniqueAudioSource.Instance?.PlaySfx(UniqueAudioSource.Sfx.Zooip);
        if (_sequence.OnLast)
        {
            text.text = "";
            UpdateActiveStates(false);

            _canRecurse = true;

            _sequence.OnFinish?.Invoke();

            if (!_recursed)
            {
                _sequence = null;
                _canRecurse = false;
            }
        }
        else
        {
            _sequence.Index++;
            text.text = _sequence.Current;
            UpdateActiveStates(true);
        }
    }

    public void UpdateActiveStates(
        bool dialogueActive)
    {
        if (_lastActiveState != dialogueActive)
        {
            _lastActiveState = dialogueActive;
            UniqueAudioSource.Instance?.PlaySfx(UniqueAudioSource.Sfx.NewHologram);
        }

        inputBlocker.SetActive(dialogueActive);
        chefHologram.SetActive(dialogueActive);
        dialogueBackground.SetActive(dialogueActive);
        navigation.SetActive(!dialogueActive);
    }
}
