﻿using UnityEngine;

public class EndScreenRenderer : MonoBehaviour
{
    [SerializeField]
    private GameObject winScreen;

    [SerializeField]
    private GameObject loseScreen;

    private void Start()
    {
        winScreen.SetActive(GameState.Instance.won);
        loseScreen.SetActive(!GameState.Instance.won);
    }
}
