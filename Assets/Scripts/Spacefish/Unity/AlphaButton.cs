﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AlphaButton :
    MonoBehaviour
{
    private Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void Start()
    {
        image.alphaHitTestMinimumThreshold = 1/300f; // 1/255 is set in inspector
    }
}
