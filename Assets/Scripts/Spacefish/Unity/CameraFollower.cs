﻿using System;
using System.Collections;
using Foggzie.Unity.Extensions;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Vector2 minCorner;
    public Vector2 maxCorner;

    private Camera _cam;

    private bool _doneFollowing;

    private CameraTarget Target
    {
        get
        {
            if (_cachedTarget.IsNullOrDestroyed())
            {
                _cachedTarget = FindObjectOfType<CameraTarget>();
            }

            return _cachedTarget;
        }
    }

    private CameraTarget _cachedTarget;

    private void Awake()
    {
        _cam = GetComponent<Camera>();
    }

    public void ZoomDramatically(
        Vector2 targetPosition,
        float targetFov,
        float secondsIn,
        AnimationCurve curveIn,
        float secondsPause,
        float secondsOut,
        AnimationCurve curveOut)
    {
        StartCoroutine(Pan(
            targetPosition,
            secondsIn,
            curveIn));
        StartCoroutine(DramaticZoom(
            targetFov,
            secondsIn,
            curveIn,
            secondsPause,
            secondsOut,
            curveOut));
    }

    private IEnumerator Pan(
        Vector3 targetPosition,
        float secondsIn,
        AnimationCurve curve)
    {
        var startPosition = transform.position;
        targetPosition.z = startPosition.z;
        var secondsLeft = secondsIn;
        while (secondsLeft > 0f)
        {
            var curveT = curve.Evaluate(1 - secondsLeft / secondsIn);
            transform.position = Vector3.Lerp(startPosition, targetPosition, curveT);

            secondsLeft -= Time.fixedUnscaledDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        transform.position = targetPosition;
    }

    private IEnumerator DramaticZoom(
        float targetFov,
        float secondsIn,
        AnimationCurve curveIn,
        float secondsPause,
        float secondsOut,
        AnimationCurve curveOut)
    {
        _doneFollowing = true;

        var startFov = _cam.fieldOfView;
        var secondsLeft = secondsIn;
        while (secondsLeft > 0f)
        {
            var curveT = curveIn.Evaluate(1 - secondsLeft / secondsIn);
            _cam.fieldOfView = Mathf.Lerp(startFov, targetFov, curveT);

            secondsLeft -= Time.fixedUnscaledDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        _cam.fieldOfView = targetFov;

        yield return new WaitForSecondsRealtime(secondsPause);

        secondsLeft = secondsOut;
        while (secondsLeft > 0f)
        {
            var curveT = curveOut.Evaluate(secondsLeft / secondsOut);
            _cam.fieldOfView = Mathf.Lerp(targetFov, startFov, curveT);

            secondsLeft -= Time.fixedUnscaledDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        _cam.fieldOfView = startFov;
    }

    private void FixedUpdate()
    {
        if (_doneFollowing) return;
        if (Target.IsNullOrDestroyed()) return;

        var maxCornerInWorldSpace = _cam.ViewportToWorldPoint(
            new Vector3(maxCorner.x, maxCorner.y, -_cam.transform.position.z));
        var minCornerInWorldSpace = _cam.ViewportToWorldPoint(
            new Vector3(minCorner.x, minCorner.y, -_cam.transform.position.z));
        var translation = GetTranslation(
            Target.transform.position, maxCornerInWorldSpace, minCornerInWorldSpace);
        _cam.transform.Translate(translation);
    }

    private static Vector2 GetTranslation(
        Vector3 targetPosition,
        Vector3 maxCornerInWorldSpace,
        Vector3 minCornerInWorldSpace)
    {
        return new Vector2(
            targetPosition.x > maxCornerInWorldSpace.x ?
                targetPosition.x - maxCornerInWorldSpace.x :
                targetPosition.x < minCornerInWorldSpace.x ?
                    targetPosition.x - minCornerInWorldSpace.x : 0f,
            targetPosition.y > maxCornerInWorldSpace.y ?
                targetPosition.y - maxCornerInWorldSpace.y :
                targetPosition.y < minCornerInWorldSpace.y ?
                    targetPosition.y - minCornerInWorldSpace.y : 0f);
    }
}
