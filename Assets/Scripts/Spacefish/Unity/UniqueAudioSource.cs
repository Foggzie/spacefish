﻿using System;
using Foggzie.Unity.Extensions;
using UnityEngine;

public class UniqueAudioSource : MonoBehaviour
{
    public static UniqueAudioSource Instance { get; private set; }

    [Serializable]
    public enum Sfx
    {
        Drawer = 0,
        FishChomp,
        FishingPoleWhoosh,
        NewHologram,
        Pop,
        Reeling,
        Wetsquish,
        Zooip
    }

    public AudioClip[] soundEffects;
    [SerializeField]
    public AudioSource sfxSource;

    private void Awake()
    {
        if (!Instance.IsNullOrDestroyed())
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void PlaySfx(Sfx sound)
    {
        sfxSource.PlayOneShot(soundEffects[(int)sound]);
    }
}
