﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UiAnimation : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;

    [SerializeField]
    private float[] delays;

    [SerializeField]
    private bool animateOnStart;

    [SerializeField]
    private bool loop;

    [SerializeField]
    private Image spriteRenderer;

    [SerializeField]
    private UnityEvent onComplete;

    [SerializeField]
    private int frameIndexEvent;

    [SerializeField]
    private UnityEvent onFrameIndexEvent;

    private void Awake()
    {
        spriteRenderer.enabled = false;
    }

    private void Start()
    {
        if (animateOnStart)
        {
            Animate();
        }
    }

    public void Animate()
    {
        spriteRenderer.enabled = true;
        StartCoroutine(AnimationRoutine());
    }

    private IEnumerator AnimationRoutine()
    {
        for (var i = 0; i < sprites.Length; ++i)
        {
            spriteRenderer.sprite = sprites[i];
            if (frameIndexEvent == i)
            {
                onFrameIndexEvent?.Invoke();
            }
            yield return new WaitForSecondsRealtime(delays[i]);
        }

        onComplete?.Invoke();

        if (loop)
        {
            Animate();
        }
    }
}
