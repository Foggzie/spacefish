﻿using UnityEngine;

public class PercentageFollower : MonoBehaviour
{
    [SerializeField]
    private float percentage = .95f;

    [SerializeField]
    private Transform target;

    private Vector3 _myStart;
    private Vector3 _targetStart;

    private void Awake()
    {
        _myStart = transform.position;
        _targetStart = target.transform.position;
    }

    private void FixedUpdate()
    {
        var targetDiff = target.position - _targetStart;
        var myDiff = targetDiff * percentage;
        transform.position = _myStart + myDiff;
    }
}
