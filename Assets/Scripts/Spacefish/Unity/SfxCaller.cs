﻿using UnityEngine;

public class SfxCaller : MonoBehaviour
{
    [SerializeField]
    private UniqueAudioSource.Sfx sound;

    public void Play()
    {
        UniqueAudioSource.Instance?.PlaySfx(sound);
    }
}
