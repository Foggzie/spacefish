﻿using UnityEngine;

public class DestroyFromCameraDistance : MonoBehaviour
{
    [SerializeField]
    private FishState fishState;

    [SerializeField]
    private float destructionDistance = 50f;

    private Transform sourceTransform;

    private void Start()
    {
        var follower = FindObjectOfType<CameraFollower>();
        sourceTransform = follower?.transform;
    }

    public void Update()
    {
        if (fishState.caught) return;

        var distance = ((Vector2)transform.position - (Vector2)sourceTransform.position).magnitude;
        if (distance >= destructionDistance)
        {
            Destroy(gameObject);
        }
    }
}
