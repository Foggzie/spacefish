﻿using System.Collections.Generic;
using Foggzie.Unity.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    public static GameState Instance;

    public int difficulty; // 0, 1, 2
    public FishDefinition targetFish;
    public FishDefinition caughtFish;

    public bool wentFishing;
    public bool won;

    public EnvironmentDefinition castEnvironment;

    public List<EnvironmentDefinition> remainingEnvironments;

    public HookDefinition hook;
    public LureDefinition lure;
    public BaitDefinition bait;

    public float secondsRemaining;

    private bool resetForExpired;

    private void Awake()
    {
        if (!Instance.IsNullOrDestroyed())
        {
            Destroy(gameObject);
            return;
        }

        Random.InitState(System.DateTime.Now.Millisecond);
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (targetFish != null)
        {
            secondsRemaining -= Time.deltaTime;
            secondsRemaining = Mathf.Max(secondsRemaining, 0f);
            if (secondsRemaining <= 0 && !resetForExpired)
            {
                resetForExpired = true;
                SceneManager.LoadScene("Ship");
            }
        }
    }

    public void SetFishy(
        FishDefinition fish)
    {
        targetFish = fish;
        secondsRemaining = 180f;
    }
}
