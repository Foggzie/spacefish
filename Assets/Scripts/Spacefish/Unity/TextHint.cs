﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TextHint : MonoBehaviour
{
    [SerializeField]
    private Text text;

    [SerializeField]
    private AnimationCurve inCurve;

    [SerializeField]
    private float timeBeforeHint;

    private Coroutine routine;

    public void Dismiss()
    {
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        var startColor = text.color;
        startColor.a = 0f;
        text.color = startColor;
        routine = StartCoroutine(HintRoutine());
    }

    private IEnumerator HintRoutine()
    {
        yield return new WaitForSecondsRealtime(timeBeforeHint);
        var secondsLeft = inCurve.keys[inCurve.length - 1].time;
        var lastColor = text.color;
        while (secondsLeft > 0)
        {
            var curveT = inCurve.Evaluate(1 - secondsLeft / inCurve.keys[inCurve.length - 1].time);
            lastColor.a = Mathf.Lerp(0f, 1f, curveT);
            text.color = lastColor;

            secondsLeft -= Time.unscaledDeltaTime;

            yield return null;
        }

        lastColor.a = 1f;
        text.color = lastColor;

        yield return new WaitForSecondsRealtime(5f);

        secondsLeft = inCurve.keys[inCurve.length - 1].time;
        lastColor = text.color;
        while (secondsLeft > 0)
        {
            var curveT = inCurve.Evaluate(1 - secondsLeft / inCurve.keys[inCurve.length - 1].time);
            lastColor.a = Mathf.Lerp(1f, 0f, curveT);
            text.color = lastColor;

            secondsLeft -= Time.unscaledDeltaTime;

            yield return null;
        }

        lastColor.a = 0f;
        text.color = lastColor;

        routine = StartCoroutine(HintRoutine());
    }

    private void OnDisable()
    {
        var color = text.color;
        color.a = 0f;
        text.color = color;
        if (routine == null) return;
        StopCoroutine(routine);
    }
}
