﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour
{
    [SerializeField]
    private float secondsToFadeIn;

    [SerializeField]
    private bool fadesIn;

    [SerializeField]
    private AnimationCurve curveIn;

    [SerializeField]
    private AnimationCurve curve;

    [SerializeField]
    private float secondsToFade;

    [SerializeField]
    private Image imageToFade;

    [SerializeField]
    private UnityEvent onComplete;

    private Coroutine fadeInRoutine;
    private Coroutine fadeOutRoutine;

    private void Start()
    {
        if (fadesIn)
        {
            fadeInRoutine = StartCoroutine(FadeInRoutine());
        }
    }

    public void StartFade()
    {
        if (fadeInRoutine != null)
        {
            StopCoroutine(fadeInRoutine);
        }
        if (fadeOutRoutine != null) return;

        fadeOutRoutine = StartCoroutine(FadeRoutine());
    }

    private IEnumerator FadeInRoutine()
    {
        var secondsLeft = secondsToFadeIn;
        var lastColor = imageToFade.color;
        while (secondsLeft > 0)
        {
            var curveT = curve.Evaluate(1 - secondsLeft / secondsToFadeIn);
            lastColor.a = Mathf.Lerp(1f, 0f, curveT);
            imageToFade.color = lastColor;

            secondsLeft -= Time.fixedUnscaledDeltaTime;

            yield return new WaitForFixedUpdate();
        }

        lastColor.a = 0f;
        imageToFade.color = lastColor;
    }

    private IEnumerator FadeRoutine()
    {
        var secondsLeft = secondsToFade;
        var lastColor = imageToFade.color;
        while (secondsLeft > 0)
        {
            var curveT = curve.Evaluate(1 - secondsLeft / secondsToFade);
            lastColor.a = Mathf.Lerp(0f, 1f, curveT);
            imageToFade.color = lastColor;

            secondsLeft -= Time.fixedUnscaledDeltaTime;

            yield return new WaitForFixedUpdate();
        }

        lastColor.a = 1f;
        imageToFade.color = lastColor;

        onComplete?.Invoke();
    }
}
