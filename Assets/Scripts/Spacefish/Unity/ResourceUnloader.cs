﻿using System.Collections;
using UnityEngine;

public class ResourceUnloader : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(UnloadRoutine());
    }

    private IEnumerator UnloadRoutine()
    {
        yield return Resources.UnloadUnusedAssets();
    }
}
