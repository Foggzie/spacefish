﻿using UnityEngine;

public class DestroyGameState : MonoBehaviour
{
    public void DoIt()
    {
        Destroy(GameState.Instance.gameObject);
    }
}
