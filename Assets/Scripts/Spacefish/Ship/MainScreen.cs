﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class MainScreen : MonoBehaviour
{
    [SerializeField]
    private Dialogue dialogue;

    private static bool TimesUp =>
        GameState.Instance.targetFish != null &&
        GameState.Instance.secondsRemaining <= 0f;

    private static bool NoTargetFishy =>
        GameState.Instance.targetFish == null;

    private static bool Reminder =>
        !GameState.Instance.wentFishing &&
        GameState.Instance.targetFish != null;

    private static bool CorrectFishy =>
        GameState.Instance.targetFish == GameState.Instance.caughtFish;

    private static bool WrongFishy =>
        GameState.Instance.wentFishing &&
        GameState.Instance.targetFish != GameState.Instance.caughtFish;

    private static bool CaughtNothing =>
        GameState.Instance.wentFishing &&
        GameState.Instance.caughtFish == null;

    private void OnEnable()
    {
        if (TimesUp)
        {
            var timesUpSequence = new Dialogue.Sequence{
                Data = new[] {
                    "You're outta time!",
                    "I guess you're not as good as they say...",
                    "Maybe I'll give you a second chance sometime."
                },
                OnFinish = () => {
                    SceneManager.LoadScene("Ending");
                }
            };
            dialogue.Display(timesUpSequence);
        }
        else if (NoTargetFishy)
        {
            HandleNoTargetFishy();
        }
        else if (Reminder)
        {
            IEnumerable<string> strings = new[] {
                $"Have you found a <b>{GameState.Instance.targetFish.displayName}</b> yet?",
            };
            if (GameState.Instance.difficulty == 0)
            {
                if (!string.IsNullOrEmpty(GameState.Instance.targetFish.hintEasy))
                {
                    strings = strings.Concat(new[] {GameState.Instance.targetFish.hintEasy});
                }
                if (!string.IsNullOrEmpty(GameState.Instance.targetFish.hintEasy2))
                {
                    strings = strings.Concat(new[] {GameState.Instance.targetFish.hintEasy2});
                }
            }
            if (GameState.Instance.difficulty == 1)
            {
                if (!string.IsNullOrEmpty(GameState.Instance.targetFish.hintMedium))
                {
                    strings = strings.Concat(new[] {GameState.Instance.targetFish.hintMedium});
                }
                if (!string.IsNullOrEmpty(GameState.Instance.targetFish.hintMedium2))
                {
                    strings = strings.Concat(new[] {GameState.Instance.targetFish.hintMedium2});
                }
            }
            if (GameState.Instance.difficulty == 2)
            {
                if (!string.IsNullOrEmpty(GameState.Instance.targetFish.hintHard))
                {
                    strings = strings.Concat(new[] {GameState.Instance.targetFish.hintHard});
                }
                if (!string.IsNullOrEmpty(GameState.Instance.targetFish.hintHard2))
                {
                    strings = strings.Concat(new[] {GameState.Instance.targetFish.hintHard2});
                }
            }
            strings = strings.Concat(new[] {"Good Luck!"});

            var reminderSequence = new Dialogue.Sequence {
                Data = strings.ToArray()
            };
            dialogue.Display(reminderSequence);
        }
        else if (CaughtNothing)
        {
            var caughtNothingSequence = new Dialogue.Sequence {
                Data = new[] {
                    "Empty handed?",
                    "You've still got time left"
                },
                OnFinish = () => {
                    GameState.Instance.wentFishing = false;
                    GameState.Instance.caughtFish = null;
                }
            };
            dialogue.Display(caughtNothingSequence);
        }
        else if (WrongFishy)
        {
            var wrongFishySequence = new Dialogue.Sequence {
                Data = new[] {
                    "Hey! That's a pretty cool fish!",
                    $"But that's a <b>{GameState.Instance.caughtFish.displayName}</b>...",
                    $"and I asked for a <b>{GameState.Instance.targetFish.displayName}</b>",
                    "Keep trying, you've got time!"
                },
                OnFinish = () => {
                    GameState.Instance.wentFishing = false;
                    GameState.Instance.caughtFish = null;
                }
            };
            dialogue.Display(wrongFishySequence);
        }
        else if (CorrectFishy)
        {
            var correctFishySequence = new Dialogue.Sequence {
                Data = new[] {
                    $"Hey! You caught a <b>{GameState.Instance.caughtFish.displayName}</b>!",
                    "Thanks so much!"
                },
                OnFinish = () => {
                    GameState.Instance.wentFishing = false;
                    GameState.Instance.caughtFish = null;
                    GameState.Instance.targetFish = null;
                    GameState.Instance.difficulty++;
                    if (GameState.Instance.difficulty > 2)
                    {
                        HandleWin();
                    }
                    else
                    {
                        HandleNextTargetFishy();
                    }
                }
            };
            dialogue.Display(correctFishySequence);
        }
    }

    private void HandleWin()
    {
        var easyFishQuest = new Dialogue.Sequence {
            Data = new[] {
                "Incredible!",
                $"You're even better than they say!",
                "You're the best fisher around!"
            },
            OnFinish = () => {
                GameState.Instance.won = true;
                SceneManager.LoadScene("Ending");
            }
        };
        dialogue.Display(easyFishQuest);
    }

    private void HandleNoTargetFishy()
    {
        var environmentIndex = Random.Range(0, GameState.Instance.remainingEnvironments.Count);
        var newEnvironment = GameState.Instance.remainingEnvironments[environmentIndex];
        var fishIndex = Random.Range(0, newEnvironment.fishies.Length);
        var newFish = newEnvironment.fishies[fishIndex];

        IEnumerable<string> strings = new[] {
            "Hello! I heard you're good at catchin' fish!",
            $"Any chance you get me a <b>{newFish.displayName}</b>?",
            $"They're found in <b>{newEnvironment.name}</b>"
        };
        if (!string.IsNullOrEmpty(newFish.hintEasy))
        {
            strings = strings.Concat(new[] {newFish.hintEasy});
        }
        if (!string.IsNullOrEmpty(newFish.hintEasy2))
        {
            strings = strings.Concat(new[] {newFish.hintEasy2});
        }
        strings = strings.Concat(new[] {"Good Luck!"});

        var easyFishQuest = new Dialogue.Sequence {
            Data = strings.ToArray(),
            OnFinish = () => {
                GameState.Instance.SetFishy(newFish);
                GameState.Instance.castEnvironment = newEnvironment;
                GameState.Instance.remainingEnvironments.Remove(newEnvironment);
            }
        };
        dialogue.Display(easyFishQuest);
    }

    private void HandleNextTargetFishy()
    {
        var environmentIndex = Random.Range(0, GameState.Instance.remainingEnvironments.Count);
        var newEnvironment = GameState.Instance.remainingEnvironments[environmentIndex];
        var fishIndex = Random.Range(0, newEnvironment.fishies.Length);
        var newFish = newEnvironment.fishies[fishIndex];

        if (GameState.Instance.difficulty == 1)
        {
            IEnumerable<string> strings = new []{
                "I've got another order for ya.",
                "Something a bit more challenging",
                $"Could you get me a <b>{newFish.displayName}</b>?",
                $"They're found in <b>{newEnvironment.name}</b>"
            };
            if (!string.IsNullOrEmpty(newFish.hintMedium))
            {
                strings = strings.Concat(new[] {newFish.hintMedium});
            }
            if (!string.IsNullOrEmpty(newFish.hintMedium2))
            {
                strings = strings.Concat(new[] {newFish.hintMedium2});
            }
            strings = strings.Concat(new[] {"Good Luck!"});

            var mediumFishQuest = new Dialogue.Sequence {
                Data = strings.ToArray(),
                OnFinish = () => {
                    GameState.Instance.SetFishy(newFish);
                    GameState.Instance.castEnvironment = newEnvironment;
                }
            };
            dialogue.Display(mediumFishQuest);
        }
        else if (GameState.Instance.difficulty == 2)
        {
            IEnumerable<string> strings = new []{
                "Alright, I've got one last order and it's tough",
                $"Could you snag a <b>{newFish.displayName}</b>?",
                $"They're found in <b>{newEnvironment.name}</b>",
            };
            if (!string.IsNullOrEmpty(newFish.hintHard))
            {
                strings = strings.Concat(new[] {newFish.hintHard});
            }
            if (!string.IsNullOrEmpty(newFish.hintHard2))
            {
                strings = strings.Concat(new[] {newFish.hintHard2});
            }
            strings = strings.Concat(new[] {"Good Luck!"});

            var hardFishQuest = new Dialogue.Sequence {
                Data = strings.ToArray(),
                OnFinish = () => {
                    GameState.Instance.SetFishy(newFish);
                    GameState.Instance.castEnvironment = newEnvironment;
                }
            };
            dialogue.Display(hardFishQuest);
        }
    }
}
