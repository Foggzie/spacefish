﻿using UnityEngine;
using UnityEngine.UI;

public class TimerToText : MonoBehaviour
{
    [SerializeField]
    private Text text;

    private void Update()
    {
        if (GameState.Instance.secondsRemaining <= 0)
        {
            text.text = "";
            return;
        }

        var minutes = Mathf.Floor(GameState.Instance.secondsRemaining / 60);
        var seconds = (int)GameState.Instance.secondsRemaining % 60;
        var secondString = seconds < 10 ? "0" + seconds : seconds.ToString();

        text.text = $"{minutes}:{secondString}";
    }
}
