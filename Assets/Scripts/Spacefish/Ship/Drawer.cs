﻿using UnityEngine;

public class Drawer : MonoBehaviour
{
    [SerializeField]
    private GameObject closed;

    [SerializeField]
    private GameObject opened;

    private void Awake()
    {
        opened.SetActive(false);
        closed.SetActive(true);
    }

    public void Open()
    {
        closed.SetActive(false);
        opened.SetActive(true);
    }

    public void Close()
    {
        closed.SetActive(true);
        opened.SetActive(false);
    }
}