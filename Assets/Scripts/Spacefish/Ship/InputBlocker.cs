﻿using UnityEngine;
using UnityEngine.Events;

public class InputBlocker : MonoBehaviour
{
    public UnityEvent onClick;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            onClick?.Invoke();
        }
        else if (Input.touchCount > 0)
        {
            onClick?.Invoke();
        }
    }
}
