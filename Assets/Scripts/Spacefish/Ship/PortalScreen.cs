﻿using UnityEngine;
using UnityEngine.UI;

public class PortalScreen : MonoBehaviour
{
    [SerializeField]
    private Button button;

    [SerializeField]
    private Image portalImage;

    private bool IsHookCrafted =>
        GameState.Instance.bait != null &&
        GameState.Instance.hook != null &&
        GameState.Instance.lure != null;

    private void Update()
    {
        if (GameState.Instance.castEnvironment != null)
        {
            portalImage.sprite = GameState.Instance.castEnvironment.portalSprite;
        }

        if (!IsHookCrafted) return;

        button.interactable = true;
    }
}
