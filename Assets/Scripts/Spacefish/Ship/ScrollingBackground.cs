﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    private BackgroundPanel[] backgrounds;

    [SerializeField]
    private float scrollTime;

    private Coroutine routine;
    private int currentBackground;
    private float backgroundWidth;

    private void Start()
    {
        foreach (var backgroundPanel in backgrounds)
        {
            backgroundPanel.DisableChildren();
        }
        backgrounds[0].EnableChildren();
        backgroundWidth = ((RectTransform)backgrounds[0].transform).rect.width;
    }

    private IEnumerator ScrollRoutine(
        bool right)
    {
        if (currentBackground == 0 && !right)
        {
            yield break;
        }

        if (currentBackground == backgrounds.Length - 1 && right)
        {
            yield break;
        }

        var lastBackground = currentBackground;
        currentBackground += right ? 1 : -1;
        backgrounds[currentBackground].EnableChildren();

        var startingX = backgrounds.Select(
            bkg => bkg.transform.localPosition.x).ToArray();
        var endingX = startingX.Select(
            start => start + (right ? -backgroundWidth : backgroundWidth)).ToArray();

        var timeLeft = scrollTime;
        while (timeLeft > 0)
        {
            for (var i = 0; i < backgrounds.Length; ++i)
            {
                backgrounds[i].transform.localPosition = new Vector3(
                    Mathf.Lerp(startingX[i], endingX[i], 1 - (timeLeft/scrollTime)),
                    backgrounds[i].transform.localPosition.y,
                    backgrounds[i].transform.localPosition.z);
            }
            timeLeft -= Time.deltaTime;
            yield return null;
        }

        for (var i = 0; i < backgrounds.Length; ++i)
        {
            backgrounds[i].transform.localPosition = new Vector3(
                endingX[i],
                backgrounds[i].transform.localPosition.y,
                backgrounds[i].transform.localPosition.z);
        }

        backgrounds[lastBackground].DisableChildren();
        routine = null;
    }

    public void ScrollLeft()
    {
        if (routine == null)
        {
            routine = StartCoroutine(ScrollRoutine(false));
        }
    }

    public void ScrollRight()
    {
        if (routine == null)
        {
            routine = StartCoroutine(ScrollRoutine(true));
        }
    }
}
