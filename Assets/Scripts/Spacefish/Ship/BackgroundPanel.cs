﻿using UnityEngine;

public class BackgroundPanel : MonoBehaviour
{
    private void Awake()
    {
        DisableChildren();
    }

    public void EnableChildren()
    {
        for (var i = 0; i < transform.childCount; ++i)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void DisableChildren()
    {
        for (var i = 0; i < transform.childCount; ++i)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
