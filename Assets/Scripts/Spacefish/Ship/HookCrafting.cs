﻿using Foggzie.Unity.Extensions;
using UnityEngine;

public class HookCrafting : MonoBehaviour
{
    [SerializeField]
    private Transform visualsAnchor;

    [SerializeField]
    private Transform lureAnchor;

    [SerializeField]
    private Transform baitAnchor;

    [SerializeField]
    private GameObject navigationButton;

    private HookPrefab _hookInstance;
    private GameObject _lureInstance;
    private GameObject _baitInstance;

    private HookDefinition Hook {
        get => GameState.Instance.hook;
        set => GameState.Instance.hook = value;
    }

    private LureDefinition Lure {
        get => GameState.Instance.lure;
        set => GameState.Instance.lure = value;
    }
    private BaitDefinition Bait {
        get => GameState.Instance.bait;
        set => GameState.Instance.bait = value;
    }

    public void OnEnable()
    {
        if (Hook != null)
        {
            SelectHook(Hook);
        }
    }

    public void SelectHook(
        HookDefinition hook)
    {
        Hook = hook;

        if (!_hookInstance.IsNullOrDestroyed()) Destroy(_hookInstance.gameObject);
        _hookInstance = Instantiate(hook.prefab, visualsAnchor).GetComponent<HookPrefab>();

        SelectLure(Lure);
        SelectBait(Bait);
    }

    public void SelectLure(
        LureDefinition lure)
    {
        Lure = lure;

        if (!_lureInstance.IsNullOrDestroyed()) Destroy(_lureInstance);
        if (lure != null)
        {
            if (!_hookInstance.IsNullOrDestroyed())
            {
                _lureInstance = Instantiate(lure.prefab, _hookInstance.LureAnchor);
            }
            else
            {
                _lureInstance = Instantiate(lure.prefab, lureAnchor);
            }
        }
    }

    public void SelectBait(
        BaitDefinition bait)
    {
        Bait = bait;

        if (!_baitInstance.IsNullOrDestroyed()) Destroy(_baitInstance);
        if (bait != null)
        {
            if (!_hookInstance.IsNullOrDestroyed())
            {
                _baitInstance = Instantiate(bait.prefab, _hookInstance.BaitAnchor);
            }
            else
            {
                _baitInstance = Instantiate(bait.prefab, baitAnchor);
            }
        }
    }

    private void Update()
    {
        navigationButton.SetActive(Hook != null && Lure != null && Bait != null);
    }
}
