﻿using UnityEngine;

public class HookPrefab : MonoBehaviour
{
    [SerializeField]
    private Transform baitAnchor;
    public Transform BaitAnchor => baitAnchor;

    [SerializeField]
    private Transform lureAnchor;
    public Transform LureAnchor => lureAnchor;
}
