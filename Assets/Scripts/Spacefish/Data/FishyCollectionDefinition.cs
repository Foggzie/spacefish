﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewFishyCollectionDefinition.asset", menuName= "spacefish/Fishy Collection Definition")]
public class FishyCollectionDefinition : ScriptableObject
{
    public FishDefinition[] fishies;
}
