﻿using UnityEngine;

[CreateAssetMenu(fileName = "NEW.hook.asset", menuName = "spacefish/Hook Definition")]
public class HookDefinition : ScriptableObject
{
    public GameObject prefab;
    public new string name;
    public GameObject simulation;
}