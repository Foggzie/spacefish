﻿using UnityEngine;

[CreateAssetMenu(fileName = "NEW.bait.asset", menuName = "spacefish/Bait Definition")]
public class BaitDefinition : ScriptableObject
{
    public GameObject prefab;
    public new string name;
    public Sprite simulationSprite;
    public Material simulationMaterial;
}
