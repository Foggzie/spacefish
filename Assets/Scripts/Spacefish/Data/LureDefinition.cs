﻿using UnityEngine;

[CreateAssetMenu(fileName = "NEW.lure.asset", menuName = "spacefish/Lure Definition")]
public class LureDefinition : ScriptableObject
{
    public GameObject prefab;
    public new string name;
    public Sprite simulationSprite;
    public Material simulationMaterial;
}