﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFishyDefinition.asset", menuName = "spacefish/Fishy Definition")]
public class FishDefinition : ScriptableObject
{
    public string displayName;

    public GameObject uiPrefab;

    public string hintEasy;
    public string hintEasy2;

    public string hintMedium;
    public string hintMedium2;

    public string hintHard;
    public string hintHard2;

    public GameObject simulationPrefab;
    public float simulationSpeed;

    public HookDefinition preferredHook;

    public LureAffinity[] lureAffinities;
    public BaitAffinity[] baitAffinities;

    [Serializable]
    public class BaitAffinity
    {
        public BaitDefinition bait;
        public float value;
    }

    [Serializable]
    public class LureAffinity
    {
        public LureDefinition lure;
        public float value;
    }
}
