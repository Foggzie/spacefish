﻿using UnityEngine;

[CreateAssetMenu(fileName = "NEW.environment.asset", menuName ="spacefish/Environment Definition")]
public class EnvironmentDefinition : ScriptableObject
{
    public Sprite background;
    public Sprite portalSprite;

    public float environmentalLightIntensity = 1f;
    public float lureLightFactor = 1f;
    public float lureLightDistance = 8f;

    public new string name;

    public FishDefinition[] fishies;
}
