﻿using Foggzie.Unity.Extensions;
using UnityEngine;

public class LureLampAdjuster : MonoBehaviour
{
    [SerializeField]
    private Light lureLight;

    private void Start()
    {
        if (GameState.Instance.IsNullOrDestroyed()) return;

        lureLight.intensity = GameState.Instance.castEnvironment.lureLightFactor;
        lureLight.range = GameState.Instance.castEnvironment.lureLightDistance;
    }
}
