﻿using UnityEngine;

public class FishState : MonoBehaviour
{
    public bool movingRight;

    public bool mouthClosed;

    public float dataSpeed;

    public bool caught;

    public FishDefinition definition;
}
