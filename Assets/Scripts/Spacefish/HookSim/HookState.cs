﻿using UnityEngine;

public class HookState : MonoBehaviour
{
    public bool bitten;
    public bool eaten;
    public bool caught;
    public HookDefinition definition;
}
