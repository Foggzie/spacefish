﻿using Foggzie.Unity.Extensions;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class BiteCollider : MonoBehaviour
{
    [SerializeField]
    private FishState fishState;

    [SerializeField]
    private Transform latchAnchor;

    private void OnTriggerEnter(Collider other)
    {
        var bittenHandler = other.gameObject.GetComponent<BittenCollisionHandler>();
        if (bittenHandler.Bitten || bittenHandler.Eaten) return;

        if (!bittenHandler.IsNullOrDestroyed())
        {
            fishState.mouthClosed = true;
            bittenHandler.Bite(latchAnchor, fishState);
        }
    }
}
