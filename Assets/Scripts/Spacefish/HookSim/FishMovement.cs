﻿using UnityEngine;

public class FishMovement : MonoBehaviour
{
    [SerializeField]
    private FishState state;

    [SerializeField]
    private Rigidbody2D body;

    [SerializeField]
    private float caughtTorque;

    [SerializeField]
    private float catchingTorque;

    [SerializeField]
    private float catchPullForce;

    [SerializeField]
    private float avoidanceTorque;

    private float attraction;

    private void Start()
    {
        foreach (var baitAffinity in state.definition.baitAffinities)
        {
            if (baitAffinity.bait == GameState.Instance.bait)
            {
                attraction += baitAffinity.value;
            }
        }

        foreach (var lureAffinity in state.definition.lureAffinities)
        {
            if (lureAffinity.lure == GameState.Instance.lure)
            {
                attraction += lureAffinity.value;
            }
        }
    }

    public void FixedUpdate()
    {
        if (state.caught)
        {
            body.AddForce(Vector2.up * 1f, ForceMode2D.Impulse);
            body.AddTorque(state.movingRight ? caughtTorque : -caughtTorque, ForceMode2D.Force);
        }
        else if (state.mouthClosed)
        {
            body.velocity = Vector2.up * catchPullForce;
            body.AddTorque(state.movingRight ? catchingTorque : -catchingTorque, ForceMode2D.Force);
        }
        else
        {
            var hookPosition = HookMovement.Instance.transform.position;
            var facingHook =
                (state.movingRight && hookPosition.x > transform.position.x) ||
                (!state.movingRight && hookPosition.x < transform.position.x);

            var facing = (Vector2)transform.right * (state.movingRight ? 1f : -1f);
            body.velocity = state.dataSpeed * facing;

            if (facingHook)
            {
                var hookDiff = hookPosition - transform.position;
                var dir = Vector3.Cross(facing, hookDiff);
                var above = dir.z > 0 ? 1f : -1f;
                if (Mathf.Abs(body.angularVelocity) < 5f)
                {
                    body.AddTorque(attraction * above * avoidanceTorque * Time.fixedDeltaTime);
                }
            }
        }
    }
}
