﻿using System.Collections;
using Foggzie.Unity.Extensions;
using UnityEngine;

public class BittenCollisionHandler : MonoBehaviour
{
    [SerializeField]
    private HookState state;

    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private float secondsSlowed = 2f;

    [SerializeField]
    private float secondsToReturnTime = 3f;

    [SerializeField]
    private AnimationCurve curveTime;

    [SerializeField]
    private AnimationCurve curveZoomIn;

    [SerializeField]
    private AnimationCurve curveZoomOut;

    [SerializeField]
    private GameObject extraLightSpriteRenderer;

    public bool Bitten => state.bitten;
    public bool Eaten => state.eaten;

    private Transform latchAnchor;
    private Vector3 lineDistance;

    public void Bite(
        Transform anchor,
        FishState fishState)
    {
        if (Bitten || Eaten) return;

        latchAnchor = anchor;

        var oldLineRendererPosition0 = lineRenderer.GetPosition(0);
        var oldLineRendererPosition1 = lineRenderer.GetPosition(1);

        lineDistance = lineRenderer.GetPosition(1) - lineRenderer.GetPosition(0);
        lineRenderer.useWorldSpace = true;
        lineRenderer.SetPosition(0, latchAnchor.position);
        lineRenderer.SetPosition(1, latchAnchor.position + lineDistance);

        state.bitten = true;

        StartCoroutine(DistortTime(
            oldLineRendererPosition0,
            oldLineRendererPosition1,
            fishState));

        var follower = FindObjectOfType<CameraFollower>();
        follower?.ZoomDramatically(
            latchAnchor.position,
            35f,
            0.15f,
            curveZoomIn,
            1.85f,
            1f,
            curveZoomOut);
    }

    private void FixedUpdate()
    {
        if (state.caught || state.bitten)
        {
            lineRenderer.useWorldSpace = true;
            lineRenderer.SetPosition(0, latchAnchor.position);
            lineRenderer.SetPosition(1, latchAnchor.position + lineDistance);
        }
    }

    private IEnumerator DistortTime(
        Vector3 oldLineRendererPosition0,
        Vector3 oldLineRendererPosition1,
        FishState fishState)
    {
        extraLightSpriteRenderer.SetActive(false);
        SetTimeScale(0.1f);
        yield return new WaitForSecondsRealtime(secondsSlowed);

        var fadeToBlack = FindObjectOfType<FadeToBlack>();
        fadeToBlack?.StartFade();

        UniqueAudioSource.Instance?.PlaySfx(UniqueAudioSource.Sfx.Reeling);
        if (fishState.definition.preferredHook == state.definition)
        {
            fishState.caught = true;
            state.caught = true;
            GameState.Instance.caughtFish = fishState.definition;
        }
        else
        {
            GameState.Instance.caughtFish = null;
            fishState.mouthClosed = false;
            state.bitten = false;
            state.eaten = true;
            lineRenderer.useWorldSpace = false;
            lineRenderer.SetPosition(0, oldLineRendererPosition0);
            lineRenderer.SetPosition(1, oldLineRendererPosition1);
        }

        var timeLeft = secondsToReturnTime;
        while (timeLeft > 0)
        {
            var curveT = curveTime.Evaluate(1 - timeLeft / secondsToReturnTime);
            SetTimeScale(Mathf.Lerp(0.1f, 1f, curveT));

            yield return new WaitForFixedUpdate();
            timeLeft -= Time.fixedUnscaledDeltaTime;
        }

        SetTimeScale(1f);
    }

    private void SetTimeScale(
        float scale)
    {
        Time.timeScale = scale;
        Time.fixedDeltaTime = Time.fixedUnscaledDeltaTime * scale;
    }
}
