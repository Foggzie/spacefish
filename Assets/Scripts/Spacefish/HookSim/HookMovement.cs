﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public class HookMovement : MonoBehaviour
{
    public static HookMovement Instance { get; private set; }

    [SerializeField]
    private HookState state;

    [SerializeField]
    private Rigidbody2D body;

    [SerializeField]
    private float forceFactor;

    [SerializeField]
    private float maxSpeed;

    private void Awake()
    {
        Assert.IsNull(Instance);
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void Move(
        Vector2 direction)
    {
        if (state.bitten)
        {
            body.velocity = Vector2.zero;
            return;
        }

        if (state.eaten)
        {
            body.AddForce(Vector2.up * 1f, ForceMode2D.Impulse);
            return;
        }

        var normalized = direction.normalized;
        var force = forceFactor * normalized;

        body.AddForce(force);
        if (body.velocity.magnitude > maxSpeed)
        {
            body.velocity = body.velocity.normalized * maxSpeed;
        }
    }
}
