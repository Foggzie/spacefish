﻿using UnityEngine;

public class FishRenderer : MonoBehaviour
{
    [SerializeField]
    private FishState state;

    [SerializeField]
    private SpriteRenderer openSprite;

    [SerializeField]
    private SpriteRenderer closedSprite;

    private bool renderStateMouthClosed;
    private bool renderStateMovingRight;

    private float xLeftOpen;
    private float xLeftClosed;

    private void Awake()
    {
        renderStateMouthClosed = false;
        closedSprite.enabled = false;
        openSprite.enabled = true;

        renderStateMovingRight = false;

        xLeftOpen = openSprite.transform.localPosition.x;
        xLeftClosed = closedSprite.transform.localPosition.x;
    }

    private void Update()
    {
        if (renderStateMouthClosed != state.mouthClosed)
        {
            closedSprite.enabled = state.mouthClosed;
            openSprite.enabled = !state.mouthClosed;

            renderStateMouthClosed = state.mouthClosed;
        }

        if (renderStateMovingRight != state.movingRight)
        {
            closedSprite.flipX = state.movingRight;
            openSprite.flipX = state.movingRight;

            var lastOpenPosition = openSprite.transform.localPosition;
            lastOpenPosition.x = state.movingRight ? -xLeftOpen : xLeftOpen;
            openSprite.transform.localPosition = lastOpenPosition;

            var lastClosedPosition = closedSprite.transform.localPosition;
            lastClosedPosition.x = state.movingRight ? -xLeftClosed : xLeftClosed;
            closedSprite.transform.localPosition = lastClosedPosition;

            renderStateMovingRight = state.movingRight;
        }
    }
}
