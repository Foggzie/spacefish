﻿using UnityEngine;

public class BackgroundRenderer : MonoBehaviour
{
    [SerializeField]
    private new SpriteRenderer renderer;

    private void Start()
    {
        if (GameState.Instance.castEnvironment == null) return;
        if (GameState.Instance.castEnvironment.background == renderer.sprite) return;

        renderer.sprite = GameState.Instance.castEnvironment.background;
    }
}
