﻿using Foggzie.Unity.Extensions;
using UnityEngine;

public class ReturnUi : MonoBehaviour
{
    public static ReturnUi Instance {get; private set;}

    private void Awake()
    {
        if (!Instance.IsNullOrDestroyed())
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        Hide();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }
}
