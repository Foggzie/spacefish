﻿using Foggzie.Unity.Extensions;
using UnityEngine;

public class DirectionalLightAdjuster : MonoBehaviour
{
    [SerializeField]
    private Light directionalLight;

    private void Start()
    {
        if (GameState.Instance.IsNullOrDestroyed()) return;

        directionalLight.intensity = GameState.Instance.castEnvironment.environmentalLightIntensity;
    }
}
