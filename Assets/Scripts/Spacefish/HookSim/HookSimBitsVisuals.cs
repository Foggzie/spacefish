﻿using UnityEngine;

public class HookSimBitsVisuals : MonoBehaviour
{
    [SerializeField]
    private HookState state;

    [SerializeField]
    private SpriteRenderer baitRenderer;

    [SerializeField]
    private SpriteRenderer lureRenderer;

    [SerializeField]
    private SpriteRenderer hookMainRenderer;

    [SerializeField]
    private SpriteRenderer hookFrontRenderer;

    private LureDefinition cachedLure;
    private BaitDefinition cachedBait;

    private void Start()
    {
        UpdateBaitVisuals();
        UpdateLureVisuals();
    }

    private void Update()
    {
        baitRenderer.enabled = !state.bitten && !state.eaten;
        lureRenderer.enabled = !state.bitten;
        hookFrontRenderer.enabled = !state.bitten;
        hookMainRenderer.enabled = !state.bitten;

        if (state.bitten || state.eaten)
        {
            return;
        }

        if (GameState.Instance.bait != cachedBait &&
                GameState.Instance.bait != null)
        {
            UpdateBaitVisuals();
        }

        if (GameState.Instance.lure != cachedLure &&
            GameState.Instance.lure != null)
        {
            UpdateLureVisuals();
        }
    }

    private void UpdateBaitVisuals()
    {
        baitRenderer.sprite = GameState.Instance.bait.simulationSprite;
        baitRenderer.material = GameState.Instance.bait.simulationMaterial;
    }

    private void UpdateLureVisuals()
    {
        lureRenderer.sprite = GameState.Instance.lure.simulationSprite;
        lureRenderer.material = GameState.Instance.lure.simulationMaterial;
    }
}
