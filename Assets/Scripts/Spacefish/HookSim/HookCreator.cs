﻿using Foggzie.Unity.Extensions;
using UnityEngine;

public class HookCreator : MonoBehaviour
{
    private HookDefinition cachedHook;
    private GameObject hook;

    private void Start()
    {
        InstantiateHook();
        GameState.Instance.wentFishing = true;
        GameState.Instance.caughtFish = null;
    }

    private void Update()
    {
        if (GameState.Instance.hook == cachedHook) return;
        if (GameState.Instance.hook == null) return;

        if (!hook.IsNullOrDestroyed()) Destroy(hook);

        InstantiateHook();
    }

    private void InstantiateHook()
    {
        cachedHook = GameState.Instance.hook;
        hook = Instantiate(cachedHook.simulation, transform);
        hook.GetComponentInChildren<HookState>().definition = cachedHook;
    }
}