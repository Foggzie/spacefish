﻿using Foggzie.Unity.Extensions;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField]
    private new Camera camera;

    private TextHint movementHint;

    private void Awake()
    {
        movementHint = FindObjectOfType<TextHint>();
    }

    private void FixedUpdate()
    {
        if (HookMovement.Instance.IsNullOrDestroyed()) return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ReturnUi.Instance?.Show();
        }

        var movementDirection = Vector2.zero;
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            movementDirection += Vector2.up;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            movementDirection += Vector2.left;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            movementDirection += Vector2.down;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            movementDirection += Vector2.right;
        }

        if (Input.GetMouseButton(0))
        {
            var hookScreenPos = camera.WorldToScreenPoint(HookMovement.Instance.transform.position);
            movementDirection = Input.mousePosition - hookScreenPos;
        }

        foreach (var touch in Input.touches)
        {
            var hookScreenPos = camera.WorldToScreenPoint(HookMovement.Instance.transform.position);
            movementDirection = touch.position - (Vector2)hookScreenPos;
        }

        if (movementDirection != Vector2.zero)
        {
            movementHint?.Dismiss();
        }
        HookMovement.Instance.Move(movementDirection);
    }
}
