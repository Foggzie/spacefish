﻿using UnityEngine;

public class WeightCompensation : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D target;

    [SerializeField]
    private Rigidbody2D[] bodies;

    private void FixedUpdate()
    {
        foreach (var body in bodies)
        {
            var compForce = body.mass * body.gravityScale * 9.81f;
            target.AddForce(Vector2.up * compForce);
        }
    }
}
