﻿using UnityEngine;

public class FishSounds : MonoBehaviour
{
    [SerializeField]
    private FishState state;

    private bool mouthClosed;

    public void Update()
    {
        if (state.mouthClosed && !mouthClosed)
        {
            mouthClosed = state.mouthClosed;
            UniqueAudioSource.Instance?.PlaySfx(UniqueAudioSource.Sfx.FishChomp);
        }
    }
}
