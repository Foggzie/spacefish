﻿
using System.Collections;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
    [SerializeField]
    private new Camera camera;

    [SerializeField]
    private float spawnTimerMin = 6f;
    [SerializeField]
    private float spawnTimerMax = 10f;

    private readonly Vector3[] cameraCorners = new Vector3[4];

    private void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    private IEnumerator SpawnRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(spawnTimerMin, spawnTimerMax));
            SpawnNewFish();
        }
    }

    private void SpawnNewFish()
    {
        // 1 2
        // 0 3
        camera.CalculateFrustumCorners(
            new Rect(0,0,1,1),
            -camera.transform.position.z,
            Camera.MonoOrStereoscopicEye.Mono,
            cameraCorners);

        var viewWidth = cameraCorners[3].x - cameraCorners[0].x;
        var viewHeight = cameraCorners[1].y - cameraCorners[0].y;

        var count = GameState.Instance.castEnvironment.fishies.Length;
        var definition = GameState.Instance.castEnvironment.fishies[Random.Range(0, count)];

        var spawnOnRight =Random.Range(0, 2) == 0;

        var spawnPosition = new Vector3(
            (spawnOnRight ? 1f : -1f) * viewWidth + 2f,
            Random.Range(-viewHeight, viewHeight),
            Random.Range(-0.1f, 0.1f));

        var spawnRotation = Quaternion.Euler(0f, 0f, Random.Range(-5f, 5f));

        var position = new Vector3(
            camera.transform.position.x + spawnPosition.x,
            camera.transform.position.y + spawnPosition.y,
            spawnPosition.z);

        var newFish = Instantiate(
                definition.simulationPrefab,
                position,
                spawnRotation);

        var state = newFish.GetComponent<FishState>();
        state.definition = definition;
        state.movingRight = !spawnOnRight;
        state.dataSpeed = Random.Range(definition.simulationSpeed - 0.5f, definition.simulationSpeed + 0.5f);
    }
}
